
THE_TAGS="v2.1.1 v2.1.0 v2.0.1 v2.0.0

delete_tags:
	git tag -d $(THE_TAGS)
	git push --delete origin $(THE_TAGS)

make_tags:
	echo $(THE_TAGS) | xargs -n1 git tag


reset_tags: delete_tags make_tags
